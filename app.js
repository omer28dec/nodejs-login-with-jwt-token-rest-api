require("dotenv").config();
require("./config/database").connect();

const auth = require("./middleware/auth");
var bcrypt = require('bcryptjs');
var jwt = require("jsonwebtoken");

const express = require("express");
const app = express();

app.use(express.json());

let User = require("./model/user");

// Register
app.post("/register", (req, res) => {
    
    try{
        const { first_name, last_name, email, password } = req.body;

        if(!(first_name && last_name && email && password)){
            res.status(400).send("All input is required");
        }

        // const old_user = User.findOne({ email });
        // if(old_user){
        //     return res.status(409).send("User already exists. Please login");
        // }

        // Create User
        encrypted_password = bcrypt.hashSync(password, 10);
        const user = User.create({
            first_name,
            last_name,
            email: email.toLowerCase(),
            password: encrypted_password
        });

        // Create Token
        const token = jwt.sign(
            {
                user_id: user.id, email
            },
            process.env.TOKEN_KEY,
            {
                expiresIn: "2h"
            }
        );

        // Save User Token
        user.token = token;

        res.status(201).json(user);
    } catch(err){
        console.log(err);
    }

});

// Login
app.post("/login", (req, res) => {
    try{
        res.setHeader('Content-Type', 'application/json');

        const { email, password } = req.body;

        if(!(email && password)){
            res.status(400).send("All input is required");
        }

        User.findOne({ email })
        .then((user) => {

            bcrypt.compare(password, user.password).then(async (match) => {
                if (!match) res.json({ error: "Wrong Password Entered!" });
                else {
                    const token = jwt.sign(
                        {
                            user_id: user.id,
                            email
                        },
                        process.env.TOKEN_KEY,
                        {
                            expiresIn: "2h"
                        }
                    );
                    user.token = token;
    
                    res.status(200).json(user);
                }
            });

        }).catch(err => res.status(400).json('Error: ' + err));

    } catch(err){
        console.log(err);
    }
});

// Welcome
app.post("/welcome", auth, (req, res) => {
    res.status(200).send("Welcome 🙌 ");
})

module.exports = app;